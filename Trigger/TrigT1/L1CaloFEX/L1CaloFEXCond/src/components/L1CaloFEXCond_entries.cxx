/*
    Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "../jFEXCondAlgo.h"
#include "../gFEXCondAlgo.h"

using namespace LVL1;

DECLARE_COMPONENT( jFEXCondAlgo )
DECLARE_COMPONENT( gFEXCondAlgo )

